from unittest import TestCase
from tests.wait import wait


class CiTest(TestCase):
    def setUp(self):
        wait(1)

    def test_case_01(self):
        self.assertTrue(True)

    def test_case_02(self):
        self.assertEqual('aaa', 'aaa')

    def test_case_03(self):
        self.assertIsNone(None)

    def test_case_error(self):
        self.fail()

    def test_case_parameterized(self):
        inputs = [
            ('001.txt', 'text/plain'),
            ('002.csv', 'text/csv'),
            ('003.json', 'application/json')
        ]

        for file_name, content_type in inputs:
            with self.subTest(file_name=file_name, content_type=content_type):
                self.assertTrue(content_type.startswith('text/'))
                wait(1)
