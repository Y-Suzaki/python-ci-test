import time


def wait(second):
    print('For avoiding excessive accesses, please wait for one second every test case.')
    time.sleep(second)
